#!/bin/zsh -eu

PREVIOUS=''
COUNT=0

for i in $( cat depthmeas.txt )
do
  if [ -z $PREVIOUS ]
  then
    PREVIOUS=$i
  else
    CURRENT=$i
    if  [[ $PREVIOUS < $CURRENT ]]
    then 
      COUNT=$((COUNT+1))
    fi
    PREVIOUS=$CURRENT
  fi
  echo "current" $COUNT
done
    
