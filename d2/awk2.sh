awk '/X/ { i += 1 } /Y/ { i += 2 } /Z/ { i += 3 } /A X|B Y|C Z/ { i += 3 } /A Y|B Z|C X/ { i += 6 } END { print i }' cheatsheet.txt

awk '/Y/ { i += 3 } /Z/ { i += 6 } /A Y|B X|C Z/ { i += 1 } /B Y|C X|A Z/ { i += 2 } /C Y|A X|B Z/ { i += 3 } END { print i }' cheatsheet.txt
